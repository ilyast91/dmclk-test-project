package tech.stepanov.dmclk;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tech.stepanov.dmclk.domain.entity.AccountEntity;
import tech.stepanov.dmclk.domain.exception.BalanceNotEnoughForDebitException;
import tech.stepanov.dmclk.domain.service.AccountOperationService;
import tech.stepanov.dmclk.domain.service.AccountService;
import tech.stepanov.dmclk.domain.service.TransferService;

@SpringBootTest
public class DmclkDomainServiceTests {

    @Autowired
    AccountService accountService;

    @Autowired
    AccountOperationService operationService;

    @Autowired
    TransferService transferService;

    @Test
    public void testCredit() {
        AccountEntity acc = accountService.create("1");
        operationService.credit(acc.getNumber(), 123.12);
    }

    @Test
    public void testDebit() {
        AccountEntity acc = accountService.create("2");
        operationService.credit(acc.getNumber(), 10.1);
        operationService.debit(acc.getNumber(), 10.0);
        Assertions.assertEquals(accountService.findOne(acc.getNumber()).getBalance(), 0.1);
    }

    @Test
    public void testNotSuccessDebit() {
        Assertions.assertThrows(BalanceNotEnoughForDebitException.class, () -> {
            AccountEntity acc = accountService.create("3");
            operationService.credit(acc.getNumber(), 9.10);
            operationService.debit(acc.getNumber(), 10.00);
        });
    }

    @Test
    public void testTransfer(){
        AccountEntity fromAcc = accountService.create("41");
        AccountEntity toAcc = accountService.create("42");

        operationService.credit(fromAcc.getNumber(), 123.12);
        operationService.credit(toAcc.getNumber(), 123.12);

        transferService.transfer(fromAcc.getNumber(), toAcc.getNumber(), 100.0);

        Assertions.assertEquals(accountService.findOne(fromAcc.getNumber()).getBalance(), 23.12);
        Assertions.assertEquals(accountService.findOne(toAcc.getNumber()).getBalance(), 223.12);
    }

    @Test
    public void testNotSuccessTransfer() {
        AccountEntity fromAcc = accountService.create("51");
        AccountEntity toAcc = accountService.create("52");

        operationService.credit(fromAcc.getNumber(), 123.12);
        operationService.credit(toAcc.getNumber(), 123.12);

        Assertions.assertThrows(BalanceNotEnoughForDebitException.class, () -> {
            transferService.transfer(fromAcc.getNumber(), toAcc.getNumber(), 124);
        });
    }
}
