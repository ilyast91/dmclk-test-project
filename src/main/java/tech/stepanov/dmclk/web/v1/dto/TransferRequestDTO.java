package tech.stepanov.dmclk.web.v1.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class TransferRequestDTO {

    @NotEmpty
    private String fromAccountNumber;

    @NotEmpty
    private String toAccountNumber;

    @NotNull
    private Double amount;
}
