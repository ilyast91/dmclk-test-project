package tech.stepanov.dmclk.web.v1.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.stepanov.dmclk.domain.exception.BalanceNotEnoughForDebitException;
import tech.stepanov.dmclk.domain.service.TransferService;
import tech.stepanov.dmclk.web.v1.dto.TransferRequestDTO;

@RestController
@RequestMapping("/api/v1/transfer")
@Tag(name = "Transfer Controller", description = "Transfer API")
public class TransferController {

    private final TransferService transferService;

    public TransferController(TransferService transferService) {
        this.transferService = transferService;
    }

    @Operation(summary = "Transfer between accounts.")
    @PostMapping
    public ResponseEntity<String> transfer(@RequestBody TransferRequestDTO dto) {
        try {
            transferService.transfer(dto.getFromAccountNumber(), dto.getToAccountNumber(), dto.getAmount());
            return ResponseEntity.ok("OK");
        } catch (BalanceNotEnoughForDebitException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getLocalizedMessage());
        }
    }
}
