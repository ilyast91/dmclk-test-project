package tech.stepanov.dmclk.web.v1.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.stepanov.dmclk.domain.entity.AccountEntity;
import tech.stepanov.dmclk.domain.entity.AccountOperationEntity;
import tech.stepanov.dmclk.domain.exception.BalanceNotEnoughForDebitException;
import tech.stepanov.dmclk.domain.service.AccountOperationService;
import tech.stepanov.dmclk.domain.service.AccountService;
import tech.stepanov.dmclk.web.v1.dto.AccountCreateDTO;
import tech.stepanov.dmclk.web.v1.dto.AccountOperationRequestDTO;
import tech.stepanov.dmclk.web.v1.dto.AccountOperationsRequestDTO;

@RestController
@RequestMapping("/api/v1/account")
@Tag(name = "Account Controller", description = "Account API")
public class AccountController {

    private final AccountService accountService;

    private final AccountOperationService accountOperationService;

    public AccountController(AccountService accountService, AccountOperationService accountOperationService) {
        this.accountService = accountService;
        this.accountOperationService = accountOperationService;
    }

    @Operation(summary = "Create account")
    @PutMapping
    public ResponseEntity<String> create(@RequestBody AccountCreateDTO dto) {
        accountService.create(dto.getAccountNumber());
        return ResponseEntity.ok("OK");
    }

    @Operation(summary = "Get account information")
    @GetMapping("/{accountNumber}")
    public ResponseEntity<AccountEntity> getOne(@PathVariable String accountNumber) {
        return ResponseEntity.ok(accountService.findOne(accountNumber));
    }

    @Operation(summary = "Debit account")
    @PostMapping("/{accountNumber}/debit")
    public ResponseEntity<String> debitAccount(@PathVariable String accountNumber,
                                               @RequestBody AccountOperationRequestDTO dto) {
        try {
            accountOperationService.debit(accountNumber, dto.getAmount());
            return ResponseEntity.ok("OK");
        } catch (BalanceNotEnoughForDebitException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getLocalizedMessage());
        }
    }

    @Operation(summary = "Credit account")
    @PostMapping("/{accountNumber}/credit")
    public ResponseEntity<String> creditAccount(@PathVariable String accountNumber,
                                                @RequestBody AccountOperationRequestDTO dto) {
        accountOperationService.credit(accountNumber, dto.getAmount());
        return ResponseEntity.ok("OK");
    }

    @Operation(summary = "Get account operations. Pageable.")
    @PostMapping("/{accountNumber}/operations")
    public ResponseEntity<Page<AccountOperationEntity>> operations(@PathVariable String accountNumber,
                                                                   @RequestBody AccountOperationsRequestDTO dto) {
        return ResponseEntity.ok(accountOperationService.operationList(accountNumber, dto.getPage(), dto.getSize()));
    }
}
