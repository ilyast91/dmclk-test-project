package tech.stepanov.dmclk.web.v1.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class AccountOperationRequestDTO {

    @NotNull
    @Min(value = 0)
    private Double amount;
}
