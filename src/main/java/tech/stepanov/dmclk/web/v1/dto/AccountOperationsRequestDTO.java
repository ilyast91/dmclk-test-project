package tech.stepanov.dmclk.web.v1.dto;

import lombok.Data;

@Data
public class AccountOperationsRequestDTO {

    private Integer page = 0;

    private Integer size = 10;
}
