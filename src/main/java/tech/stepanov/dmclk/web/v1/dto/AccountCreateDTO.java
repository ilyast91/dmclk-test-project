package tech.stepanov.dmclk.web.v1.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class AccountCreateDTO {

    @NotEmpty
    private String accountNumber;
}