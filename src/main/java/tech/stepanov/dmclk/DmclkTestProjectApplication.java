package tech.stepanov.dmclk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DmclkTestProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(DmclkTestProjectApplication.class, args);
    }

}
