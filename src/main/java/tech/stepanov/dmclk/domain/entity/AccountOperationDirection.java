package tech.stepanov.dmclk.domain.entity;

public enum AccountOperationDirection {
    DEBIT,
    CREDIT
}
