package tech.stepanov.dmclk.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tp_account_operation")
public class AccountOperationEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, unique = true)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", nullable = false)
    @JsonIgnore
    private AccountEntity account;

    @Column(name = "direction", length = 7, nullable = false)
    @Enumerated(EnumType.STRING)
    private AccountOperationDirection direction;

    @Column(name = "amount", scale = 2, nullable = false)
    private Double amount;

    @Column(name = "created_at")
    private Date createdAt;

    @PrePersist
    public void onPrePersist() {
        this.createdAt = new Date();
    }
}
