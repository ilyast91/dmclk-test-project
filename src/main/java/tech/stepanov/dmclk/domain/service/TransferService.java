package tech.stepanov.dmclk.domain.service;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class TransferService {

    private final AccountOperationService accountOperationService;

    public TransferService(AccountOperationService accountOperationService) {
        this.accountOperationService = accountOperationService;
    }

    @Transactional
    public void transfer(String fromAccountNumber, String toAccountNumber, double amount) {
        accountOperationService.debit(fromAccountNumber, amount);
        accountOperationService.credit(toAccountNumber, amount);
    }
}
