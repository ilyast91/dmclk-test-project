package tech.stepanov.dmclk.domain.service;

import org.springframework.stereotype.Service;
import tech.stepanov.dmclk.domain.entity.AccountEntity;
import tech.stepanov.dmclk.domain.exception.ResourceNotFoundException;
import tech.stepanov.dmclk.domain.repo.AccountRepo;

import javax.transaction.Transactional;

@Service
public class AccountService {

    private final AccountRepo accountRepo;

    public AccountService(AccountRepo accountRepo) {
        this.accountRepo = accountRepo;
    }

    @Transactional
    public AccountEntity create(String accountNumber) {
        AccountEntity acc = new AccountEntity();
        acc.setNumber(accountNumber);
        accountRepo.save(acc);
        return acc;
    }

    public AccountEntity findOne(String number) {
        return accountRepo.findAccountByNumber(number).orElseThrow(()->new ResourceNotFoundException("Account", "Number", number));
    }
}
