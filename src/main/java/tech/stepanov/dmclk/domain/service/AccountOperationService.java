package tech.stepanov.dmclk.domain.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import tech.stepanov.dmclk.domain.entity.AccountEntity;
import tech.stepanov.dmclk.domain.entity.AccountOperationDirection;
import tech.stepanov.dmclk.domain.entity.AccountOperationEntity;
import tech.stepanov.dmclk.domain.exception.BalanceNotEnoughForDebitException;
import tech.stepanov.dmclk.domain.repo.AccountOperationRepo;
import tech.stepanov.dmclk.domain.repo.AccountRepo;

import javax.transaction.Transactional;

@Service
public class AccountOperationService {

    private final AccountRepo accountRepo;
    private final AccountOperationRepo accountOperationRepo;
    private final AccountService accountService;

    public AccountOperationService(AccountRepo accountRepo, AccountOperationRepo accountOperationRepo, AccountService accountService) {
        this.accountRepo = accountRepo;
        this.accountOperationRepo = accountOperationRepo;
        this.accountService = accountService;
    }

    public Page<AccountOperationEntity> operationList(String accountNumber, int page, int size) {
        AccountEntity acc = accountService.findOne(accountNumber);
        return accountOperationRepo.findAllByAccountNumber(acc.getNumber(), PageRequest.of(page, size));
    }

    @Transactional
    public void debit(String accountNumber, double amount) {
        AccountEntity acc = accountService.findOne(accountNumber);
        if (acc.getBalance() < amount) throw new BalanceNotEnoughForDebitException();

        AccountOperationEntity accOperation = new AccountOperationEntity();
        accOperation.setAccount(acc);
        accOperation.setDirection(AccountOperationDirection.DEBIT);
        accOperation.setAmount(amount);
        accountOperationRepo.save(accOperation);

        double balance = acc.getBalance() - amount;
        double result = (double) Math.round(balance * 100) / 100;
        acc.setBalance(result);

        accountRepo.save(acc);
    }

    @Transactional
    public void credit(String accountNumber, double amount) {
        AccountEntity acc = accountService.findOne(accountNumber);

        AccountOperationEntity accOperation = new AccountOperationEntity();
        accOperation.setAccount(acc);
        accOperation.setDirection(AccountOperationDirection.CREDIT);
        accOperation.setAmount(amount);
        accountOperationRepo.save(accOperation);

        double balance = acc.getBalance() + amount;
        double result = (double) Math.round(balance * 100) / 100;
        acc.setBalance(result);
        accountRepo.save(acc);
    }
}
