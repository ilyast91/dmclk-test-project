package tech.stepanov.dmclk.domain.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ResourceNotFoundException extends RuntimeException {

    private String field;
    private Object value;

    public ResourceNotFoundException(String resource, String field, Object value) {
        super(String.format("Resource '%s' not found with %s: '%s'", resource, field, value));
        this.field = field;
        this.value = value;
    }
}
