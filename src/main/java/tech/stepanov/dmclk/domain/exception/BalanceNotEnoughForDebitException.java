package tech.stepanov.dmclk.domain.exception;

public class BalanceNotEnoughForDebitException extends RuntimeException {

    public BalanceNotEnoughForDebitException() {
        super("Account balance not enough for debit");
    }
}
