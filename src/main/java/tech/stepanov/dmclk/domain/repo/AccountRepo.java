package tech.stepanov.dmclk.domain.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import tech.stepanov.dmclk.domain.entity.AccountEntity;

import java.util.Optional;

@Repository
public interface AccountRepo extends JpaRepository<AccountEntity, Long>, JpaSpecificationExecutor<AccountEntity> {

    Optional<AccountEntity> findAccountByNumber(String number);
}
