package tech.stepanov.dmclk.domain.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import tech.stepanov.dmclk.domain.entity.AccountOperationEntity;

import java.util.List;

@Repository
public interface AccountOperationRepo extends JpaRepository<AccountOperationEntity, Long>, JpaSpecificationExecutor<AccountOperationEntity> {

    Page<AccountOperationEntity> findAllByAccountNumber(String number, Pageable pageable);

    List<AccountOperationEntity> findAllByAccountNumber(String number);
}
