# Domclick test project 

Модель БД:
![alt text](./db-model.png)

### Сборка проекта
> mvn clean package

### Запуск приложения
> java -jar ./target/dmclk-0.0.1-SNAPSHOT.jar
> 
### Пример запуска приложения на другом порту (8081)
> java -jar -DPORT=8081 ./target/dmclk-0.0.1-SNAPSHOT.jar

### Swagger url:
> http://localhost:8080/swagger-ui.html

###API :
 - POST /api/v1/transfer
 - PUT /api/v1/account
 - POST /api/v1/account/{accountNumber}/operations
 - POST /api/v1/account/{accountNumber}/debit
 - POST /api/v1/account/{accountNumber}/credit
 - GET /api/v1/account/{accountNumber}